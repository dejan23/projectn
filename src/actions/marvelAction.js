import axios from 'axios';
import { FETCH_ITEMS, FETCH_ITEMS_SUCCESS, FETCH_ITEMS_FAIL } from './types';

export const fetchItems = page => async dispatch => {
  dispatch({ type: FETCH_ITEMS });
  try {
    const response = await axios.get(
      `https://jsonplaceholder.typicode.com/photos?_limit=20&_page=${page}`
    );

    dispatch({ type: FETCH_ITEMS_SUCCESS, payload: response });
  } catch (e) {
    dispatch({ type: FETCH_ITEMS_FAIL });
  }
};

export const fetchItem = value => async dispatch => {
  dispatch({ type: FETCH_ITEMS });

  try {
    const response = await axios.get(
      `https://jsonplaceholder.typicode.com/photos?id=${value}`
    );

    dispatch({ type: FETCH_ITEMS_SUCCESS, payload: response });
  } catch (e) {
    dispatch({ type: FETCH_ITEMS_FAIL });
  }
};

export const saveFavorite = item => async dispatch => {
  if (localStorage.getItem('favorites')) {
    const favorites = JSON.parse(localStorage.getItem('favorites'));

    if (favorites.length >= 5) return null;

    const found = favorites.find(favorite => {
      return item.id === favorite.id;
    });

    if (found) return null;

    const arr = [...favorites, item];
    localStorage.setItem('favorites', JSON.stringify(arr));
  } else {
    const arr = [item];
    localStorage.setItem('favorites', JSON.stringify(arr));
  }
};

export const deleteFavorite = item => async dispatch => {
  const favorites = JSON.parse(localStorage.getItem('favorites'));

  if (!favorites) return 'No favorites';

  const newFavorites = favorites.filter(value => value.id != item.id);

  localStorage.setItem('favorites', JSON.stringify(newFavorites));
};
