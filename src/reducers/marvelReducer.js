import {
  FETCH_ITEMS,
  FETCH_ITEMS_SUCCESS,
  FETCH_ITEMS_FAIL
} from '../actions/types';

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_ITEMS:
      return { ...state, loading: true };
    case FETCH_ITEMS_SUCCESS:
      return { ...state, items: action.payload, loading: false };
    case FETCH_ITEMS_FAIL:
      return {
        ...state,
        error: 'There was and error, please try again',
        loading: false
      };

    default:
      return state;
  }
};
