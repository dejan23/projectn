import marvelReducer from './marvelReducer';

const rootReducer = {
  marvel: marvelReducer
};

export default rootReducer;
