import React from 'react';

const Favorites = props => {
  const arr = JSON.parse(localStorage.getItem('favorites'));
  let msg = null;

  if (!arr || !arr.length) {
    msg = 'Click on the item to save it to Favorites';
  }

  if (arr && arr.length >= 5)
    msg =
      '5 items are max, please remove some if you want to add new favorite item';

  return (
    <div className="favorites">
      <h1>Favorites</h1>

      <div className="favorites_container">
        <div className="favorites_message">{msg}</div>
        <div className="favorite_items">
          {arr &&
            arr.map(item => {
              return (
                <div
                  onClick={() => props.handleDeleteFavorite(item)}
                  className="item_box"
                  key={item.id}
                >
                  <img src={item.thumbnailUrl} />
                  <div className="item_box_text-content">
                    <div>
                      <label>ID:</label> {item.id}
                    </div>
                    <div>
                      <label>Title:</label> {item.title}
                    </div>
                  </div>
                  <div className="favorites_bookmark">
                    Click to remove from favorites
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </div>
  );
};

export default Favorites;
