import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import Loader from './Loader';
import Favorites from './Favorites';

class Homepage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1
    };

    this.props.fetchItems(1);
  }

  handleSearch = async e => {
    const { value } = e.target;
    if (!value) return this.props.fetchItems(this.state.currentPage);

    this.props.fetchItem(value);
  };

  handleNext = async () => {
    await this.setState({ currentPage: this.state.currentPage + 1 });
    this.props.fetchItems(this.state.currentPage);
  };

  handlePrevious = async () => {
    if (this.state.currentPage <= 1) return;
    await this.setState({ currentPage: this.state.currentPage - 1 });
    this.props.fetchItems(this.state.currentPage);
  };

  handleFavorite = item => {
    this.props.saveFavorite(item);
    this.setState({ saved: true });
  };

  handleDeleteFavorite = item => {
    this.props.deleteFavorite(item);
    this.setState({ saved: true });
  };

  renderItems() {
    let arr = JSON.parse(localStorage.getItem('favorites'));
    if (!arr) arr = [];

    if (this.props.items.data.length === 0) {
      return <div>No results found</div>;
    }

    return (
      <div
        style={{ opacity: this.props.loading ? '0.5' : '1' }}
        className="item_container"
      >
        {this.props.items.data.map(item => {
          let saveText = 'Click to save';

          const found = arr.find(localItem => {
            return localItem.id === item.id;
          });

          if (found) saveText = 'Saved';

          return (
            <div
              onClick={() => this.handleFavorite(item)}
              className="item_box"
              key={item.id}
            >
              <img src={item.thumbnailUrl} />
              <div className="item_box_text-content">
                <div>
                  <label>ID:</label> {item.id}
                </div>
                <div>
                  <label>Title:</label> {item.title}
                </div>
              </div>
              <div className="bookmark">{saveText}</div>
            </div>
          );
        })}
        <div
          className="item_box_buttons"
          style={{
            display:
              this.props.items && this.props.items.data.length <= 1 && 'none'
          }}
        >
          <button
            disabled={this.state.currentPage <= 1}
            onClick={this.handlePrevious}
          >
            Previous
          </button>
          <span> - {this.state.currentPage} - </span>
          <button onClick={this.handleNext}>Next</button>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="homepage">
        <div className="homepage__wrapper">
          <div>
            <h1>Project N</h1>
            {this.props.error && (
              <span style={{ color: 'red' }}>{this.props.error}</span>
            )}
          </div>

          <div className="homepage__search-box">
            <input
              placeholder="Search by ID"
              onChange={this.handleSearch}
              type="text"
            />
          </div>

          <Favorites handleDeleteFavorite={this.handleDeleteFavorite} />

          {!this.props.items ? <Loader width={'30px'} /> : this.renderItems()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ marvel }) => {
  return {
    items: marvel.items,
    loading: marvel.loading,
    error: marvel.error
  };
};

export default connect(
  mapStateToProps,
  actions
)(Homepage);
