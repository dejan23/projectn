import React from 'react';

const Loader = ({ width }) => (
  <div className="loader">
    <img style={{ width }} className="loader__image" src="/images/loader.gif" />
  </div>
);

export default Loader;
