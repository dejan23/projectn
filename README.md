Project N
==================================

## Instructions

**Development:**

* npm i
* npm run build:dev
* npm run dev (it is gonna run on localhost:8080)

**Production:**

* npm i
* npm run build:prod

==================================

***Notes***

* Since Marvel Developer API is not working, I used https://jsonplaceholder.typicode.com/photos as instructed;
* That said, you can not search ie. for marvel characters instead search finds dummy items from given API endpoint;
* When component loads endpoint is called to load all results, but with pagination, limit is set to 20 results per page - you have ability to go back and forward through pages;
* You can click on result items and save them to favorites, also you can remove them from favorites. (maximum items in favorites is set to 5) - you can not add same item twice, the text "click to save" will change to "saved" when you click on the item and change back wehn removed from favorites;
* You can search by ID, just randomly type some number and results will be usually shown in a second. Delete everything from the search field and you will get 
search results as it was when component first mounted;
